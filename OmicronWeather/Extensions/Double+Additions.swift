//
//  Double+Additions.swift
//  OmicronWeather
//
//  Created by Gene Crucean on 6/15/19.
//  Copyright © 2019 Gene Crucean. All rights reserved.
//

import Foundation

extension Double {
    func getDayStringFromUTC() -> String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"

        return dateFormatter.string(from: date)
    }
}
