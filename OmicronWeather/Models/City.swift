//
//  City.swift
//  OmicronWeather
//
//  Created by Gene Crucean on 6/15/19.
//  Copyright © 2019 Gene Crucean. All rights reserved.
//

import Foundation

public struct City: Codable {
    public let id: Int
    public let name: String
    public let coord: Coord
    public let country: String
    public let population: Int
    public let timezone: Int
}
