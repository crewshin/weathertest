//
//  DaysWeather.swift
//  OmicronWeather
//
//  Created by Gene Crucean on 6/15/19.
//  Copyright © 2019 Gene Crucean. All rights reserved.
//

import Foundation

public struct DaysWeather: Codable {
    public let dt: Int
    public let temp: Temp
    public let pressure: Double
    public let humidity: Int
    public let weather: [WeatherMetadata]
    public let speed: Double
    public let deg: Int
    public let clouds: Int
    public let rain: Double?
    public let snow: Double?
}
