//
//  Coord.swift
//  OmicronWeather
//
//  Created by Gene Crucean on 6/15/19.
//  Copyright © 2019 Gene Crucean. All rights reserved.
//

import Foundation

public struct Coord: Codable {
    public let lat: Double
    public let lon: Double
}
