//
//  Weather.swift
//  OmicronWeather
//
//  Created by Gene Crucean on 6/15/19.
//  Copyright © 2019 Gene Crucean. All rights reserved.
//

import Foundation

public struct Weather: Codable {
    public let city: City
    public let cod: String
    public let message: Double
    public let cnt: Int
    public let list: [DaysWeather]
}
