//
//  WeatherMetadata.swift
//  OmicronWeather
//
//  Created by Gene Crucean on 6/15/19.
//  Copyright © 2019 Gene Crucean. All rights reserved.
//

import Foundation

public struct WeatherMetadata: Codable {
    public let id: Int
    public let main: String
    public let description: String
    public let icon: String
}
