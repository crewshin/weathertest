//
//  WeatherHomeDetailViewController.swift
//  OmicronWeather
//
//  Created by Gene Crucean on 6/15/19.
//  Copyright © 2019 Gene Crucean. All rights reserved.
//

import UIKit
import SDWebImage

class WeatherHomeDetailViewController: UIViewController {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!

    // Temp
    @IBOutlet weak var tempDayLabel: UILabel!
    @IBOutlet weak var tempDayValueLabel: UILabel!
    @IBOutlet weak var tempMinLabel: UILabel!
    @IBOutlet weak var tempMinValueLabel: UILabel!
    @IBOutlet weak var tempMaxLabel: UILabel!
    @IBOutlet weak var tempMaxValueLabel: UILabel!
    @IBOutlet weak var tempNightLabel: UILabel!
    @IBOutlet weak var tempNightValueLabel: UILabel!
    @IBOutlet weak var tempEveLabel: UILabel!
    @IBOutlet weak var tempEveValueLabel: UILabel!
    @IBOutlet weak var tempMornLabel: UILabel!
    @IBOutlet weak var tempMornValueLabel: UILabel!

    // Main
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var pressureValueLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var humidityValueLabel: UILabel!
    @IBOutlet weak var weatherMainLabel: UILabel!
    @IBOutlet weak var weatherMainValueLabel: UILabel!
    @IBOutlet weak var weatherDescriptionValueLabel: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var speedValueLabel: UILabel!
    @IBOutlet weak var cloudLabel: UILabel!
    @IBOutlet weak var cloudsValueLabel: UILabel!
    @IBOutlet weak var degLabel: UILabel!
    @IBOutlet weak var degValueLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var rainValueLabel: UILabel!
    @IBOutlet weak var snowLabel: UILabel!
    @IBOutlet weak var snowValueLabel: UILabel!

    var daysWeather: DaysWeather?

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Omicron Weather"

        configureUI()
    }

    func configureUI() {
        guard let day = daysWeather else { return }

        dayLabel.text = Double(day.dt).getDayStringFromUTC()

        tempDayValueLabel.text = "\(Int(day.temp.day))"
        tempMinValueLabel.text = "\(Int(day.temp.min))"
        tempMaxValueLabel.text = "\(Int(day.temp.max))"
        tempNightValueLabel.text = "\(Int(day.temp.night))"
        tempEveValueLabel.text = "\(Int(day.temp.eve))"
        tempMornValueLabel.text = "\(Int(day.temp.morn))"

        pressureValueLabel.text = "\(Int(day.pressure))"
        humidityValueLabel.text = "\(day.humidity)"
        weatherMainValueLabel.text = day.weather.first?.main ?? "ERROR"
        weatherDescriptionValueLabel.text = day.weather.first?.description.capitalized ?? "ERROR"
        speedValueLabel.text = "\(Int(day.speed))"
        cloudsValueLabel.text = "\(day.clouds)"
        degValueLabel.text = "\(day.deg)"
        rainValueLabel.text = "\(day.rain ?? 0.0)in"
        snowValueLabel.text = "\(day.snow ?? 0.0)in"

        let imageFileName = day.weather.first?.icon ?? "01d"
        mainImageView.sd_setImage(with: URL(string: "https://openweathermap.org/img/w/\(imageFileName).png"), completed: nil)
    }
}
