//
//  WeatherHomeCommunicator.swift
//  OmicronWeather
//
//  Created by Gene Crucean on 6/15/19.
//  Copyright © 2019 Gene Crucean. All rights reserved.
//

import UIKit

public enum WeatherHomeCommunicatorError: Error {
    case searchQueryFailedToEncode(String)
    case unauthorized(String)
    case genericNon200(String)
    case notSerializable(String)
    case notDictionary(String)
    case dataIsNil(String)
    case requestTimeout(String)
}

class WeatherHomeCommunicator {

    private let headers = [
        "Accept": "application/json",
        "Content-Type": "application/json"
    ]

    /// Fetches weather data from the API.
    ///
    /// - Parameters:
    ///     - for (string): The term which is searched for weather data.
    ///     - completion (Weather?, WeatherHomeCommunicatorError?): Returns optional Weather and Error objects.
    func getWeather(for searchTerm: String, completion: @escaping ([String: Any]?, WeatherHomeCommunicatorError?) -> Void) {

        guard let searchTermEncoded = searchTerm.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            completion(nil, .searchQueryFailedToEncode("Search query failed to encode."))
            return
        }

        let endpoint = "https://api.openweathermap.org/data/2.5/forecast/daily?q=\(searchTermEncoded)&mode=json&units=imperial&cnt=14&appid=f8fdae74c29544baebdb927d392c5538"

        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)

        let url = URL(string: endpoint)!
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in

            guard let httpResponse = response as? HTTPURLResponse else { return }

            // Error checking.
            if httpResponse.statusCode == 401 {
                completion(nil, .unauthorized("Unauthorized."))
            } else if httpResponse.statusCode == 408 {
                completion(nil, .requestTimeout("The request timed out. Please try again."))
                return
            } else if httpResponse.statusCode != 200 || error != nil {
                completion(nil, .genericNon200("Something random ham jammed."))
                return
            }

            // Make sure we have data.
            guard let data = data else {
                completion(nil, .dataIsNil("Response didn't contain any data."))
                return
            }

            // Decode to Weather object.
            do {
                // Happy path.
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    completion(json, nil)
                    return
                } else {
                    completion(nil, .notDictionary("Response was not a (required) dictionary."))
                    return
                }
            } catch {
                completion(nil, .notSerializable("Response was not serializable."))
                return
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
