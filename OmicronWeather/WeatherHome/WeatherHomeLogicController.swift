//
//  WeatherHomeLogicController.swift
//  OmicronWeather
//
//  Created by Gene Crucean on 6/15/19.
//  Copyright © 2019 Gene Crucean. All rights reserved.
//

import UIKit

public class WeatherHomeLogicController: NSObject {

    public static let shared = WeatherHomeLogicController()
    private let communicator: WeatherHomeCommunicator

    init(communicator: WeatherHomeCommunicator = WeatherHomeCommunicator()) {
        self.communicator = communicator
        super.init()
    }

    public func getWeather(for searchTerm: String, completion: @escaping (Weather?, WeatherHomeCommunicatorError?) -> Void) {
        communicator.getWeather(for: searchTerm) { [weak self] (weather, _) in
            // Decode to Weather object.
            guard let weather = weather else {
                completion(nil, .notSerializable("Response was not serializable."))
                return
            }
            completion(self?.convertToWeatherObject(from: weather), nil)
        }
    }

    public func convertToWeatherObject(from json: [String: Any]) -> Weather? {
        guard let data = try? JSONSerialization.data(withJSONObject: json, options: []) else { return nil }

        let weather = try? JSONDecoder().decode(Weather.self, from: data)

        return weather
    }
}
