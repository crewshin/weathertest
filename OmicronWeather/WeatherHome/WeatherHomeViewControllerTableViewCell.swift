//
//  WeatherHomeViewControllerTableViewCell.swift
//  OmicronWeather
//
//  Created by Gene Crucean on 6/15/19.
//  Copyright © 2019 Gene Crucean. All rights reserved.
//

import UIKit

class WeatherHomeViewControllerTableViewCell: UITableViewCell {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var highLabel: UILabel!
    @IBOutlet weak var lowLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(with day: DaysWeather) {
        dayLabel.text = Double(day.dt).getDayStringFromUTC()
        highLabel.text = "\(Int(day.temp.max))"
        lowLabel.text = "\(Int(day.temp.min))"
        humidityLabel.text = String(format: NSLocalizedString("HOME_HUMIDITY", comment: "String showing the days humidity"), day.humidity)

        let imageFileName = day.weather.first?.icon ?? "01d"
        weatherImageView.sd_setImage(with: URL(string: "https://openweathermap.org/img/w/\(imageFileName).png"), completed: nil)
    }
}
