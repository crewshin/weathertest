//
//  WeatherHomeViewController.swift
//  OmicronWeather
//
//  Created by Gene Crucean on 6/15/19.
//  Copyright © 2019 Gene Crucean. All rights reserved.
//

import UIKit
import SDWebImage

class WeatherHomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var mainHiderView: UIView!
    var hiderView = UIView()
    let viewModel = WeatherHomeViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Omicron Weather"

        tableView.delegate = self
        tableView.dataSource = self

        mainHiderView.isHidden = false

        fetchWeather()
    }

    // MARK: - Setup

    func setupHeader() {
        viewModel.getImageForHeader(with: mainImageView)
    }

    func fetchWeather() {
        viewModel.getWeather(for: "Winter Park") { [weak self] (_, error) in
            // Handle errors
            if let error = error {
                self?.handleNetworkErrors(for: error)
                return
            }

            DispatchQueue.main.async {
                self?.toggleHiderViewVisibility()
                self?.tableView.reloadData()
                self?.setupHeader()
            }
        }
    }

    func toggleHiderViewVisibility() {
        if mainHiderView.isHidden {
            mainHiderView.isHidden = false
        } else {
            mainHiderView.isHidden = true
        }
    }

    // MARK: - Segue

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? WeatherHomeDetailViewController, segue.identifier == "dayDetailSegue" {
            vc.daysWeather = viewModel.daysWeatherToView

            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
        }
    }

    // MARK: - Error Handling

    private func handleNetworkErrors(for error: WeatherHomeCommunicatorError?) {
        if let error = error {
            switch error {
            case .genericNon200(let msg):
                showErrorAlert(message: msg)
            case .searchQueryFailedToEncode(let msg):
                showErrorAlert(message: msg)
            case .unauthorized(let msg):
                showErrorAlert(message: msg)
            case .notDictionary(let msg):
                showErrorAlert(message: msg)
            case .notSerializable(let msg):
                showErrorAlert(message: msg)
            case .dataIsNil(let msg):
                showErrorAlert(message: msg)
            case .requestTimeout(let msg):
                showErrorAlert(message: msg)
            }
        }
    }

    private func showErrorAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDelegate and DataSource
extension WeatherHomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.weatherData?.list.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let daysWeather = viewModel.weatherData?.list[indexPath.row] else { return UITableViewCell() }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as? WeatherHomeViewControllerTableViewCell else { return UITableViewCell() }

        cell.configure(with: daysWeather)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        guard let daysWeather = viewModel.weatherData?.list[indexPath.row] else { return }
        viewModel.daysWeatherToView = daysWeather

        performSegue(withIdentifier: "dayDetailSegue", sender: self)
    }
}
