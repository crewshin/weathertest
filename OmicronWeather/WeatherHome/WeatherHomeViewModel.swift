//
//  WeatherHomeViewModel.swift
//  OmicronWeather
//
//  Created by Gene Crucean on 6/15/19.
//  Copyright © 2019 Gene Crucean. All rights reserved.
//

import UIKit

class WeatherHomeViewModel: NSObject {

    let logicController: WeatherHomeLogicController

    var weatherData: Weather?
    var daysWeatherToView: DaysWeather?

    init(logicController: WeatherHomeLogicController = .shared) {
        self.logicController = logicController
        super.init()
    }

    func getWeather(for searchTerm: String, completion: @escaping (Weather?, WeatherHomeCommunicatorError?) -> Void) {
        logicController.getWeather(for: searchTerm) { [weak self] (weather, error) in
            self?.weatherData = weather
            completion(weather, error)
        }
    }

    func getImageForHeader(with imageView: UIImageView) {
        let imageFileName = weatherData?.list.first?.weather.first?.icon ?? "01d"
        imageView.sd_setImage(with: URL(string: "https://openweathermap.org/img/w/\(imageFileName).png"), completed: nil)
    }
}
